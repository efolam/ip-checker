require('./bootstrap')

import Vue from "vue"

// Registar all components globally
import components from "./components"
for (let component in components) {
  Vue.component(component, components[component])
}

new Vue({
  el: '#app'
})
