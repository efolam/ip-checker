import AppField from "./AppField"
import CheckIp from "./CheckIp"
import LoginForm from "./LoginForm"
import RegisterForm from "./RegisterForm"

export default {
  AppField,
  CheckIp,
  LoginForm,
  RegisterForm
}
