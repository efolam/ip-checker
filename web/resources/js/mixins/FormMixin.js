export default {
  props: {
    action: {
      type: String,
      default: ''
    }
  },

  data() {
    return {
      fields: {},
      hiddenData: {},
      sending: false,
      success: false,
      result: {},
      errors: {},
      redirectTo: ''
    }
  },

  methods: {
    submit() {
      if (this.sending) {
        return false
      }

      this.sending = true

      this.sendForm()
    },
    sendForm() {
      this.success = false
      this.errors = {}
      this.message = ''

      axios.post(this.action, { ...this.fields, ...this.hiddenData })
        .then(response => {
          this.sending = false
          this.success = true
          this.result = response.data
          this.redirect()
        })
        .catch(error => {
          this.sending = false
          this.result = {}
          this.errors = error.response.data.errors
        })
    },
    redirect() {
      if (this.redirectTo && this.success) {
        window.location.replace(this.redirectTo)
      }
    }
  }
}
