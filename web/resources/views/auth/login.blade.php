<x-master>
  <main class="page container">
    <div class="title">Авторизация:</div>

    <login-form action="{{ route('login') }}"></login-form>
  </main>
</x-master>
