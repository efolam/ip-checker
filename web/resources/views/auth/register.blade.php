<x-master>
  <main class="page container">
    <div class="title">Регистрация:</div>

    <register-form action="{{ route('register') }}"></register-form>
  </main>
</x-master>
