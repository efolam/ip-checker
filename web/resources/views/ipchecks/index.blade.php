<x-master>
  <main class="page page-home container">
    <div class="title">Список запросов по IP:</div>

    <table>
      <tr>
        <th>IP Клиента</th>
        <th>Целевой IP</th>
        <th>Город</th>
        <th>Страна</th>
        <th>Дата проверки</th>
      </tr>

      @foreach ($ipChecks as $ipCheck)
        <tr>
          <td>{{ $ipCheck->client_ip }}</td>
          <td>{{ $ipCheck->target_ip }}</td>
          <td>{{ $ipCheck->city }}</td>
          <td>{{ $ipCheck->country }}</td>
          <td>{{ $ipCheck->created_at }}</td>
        </tr>
      @endforeach
    </table>

    <div class="table-mobile">
      @foreach ($ipChecks as $ipCheck)
        <ul class="table-mobile__row">
          <li class="table-mobile__column"><b>IP Клиента:</b> {{ $ipCheck->client_ip }}</li>
          <li class="table-mobile__column"><b>Целевой IP:</b> {{ $ipCheck->target_ip }}</li>
          <li class="table-mobile__column"><b>Город:</b> {{ $ipCheck->city }}</li>
          <li class="table-mobile__column"><b>Страна:</b> {{ $ipCheck->country }}</li>
          <li class="table-mobile__column"><b>Дата проверки:</b> {{ $ipCheck->created_at }}</li>
        </ul>
      @endforeach
    </div>
  </main>
</x-master>
