<nav class="navigation">
  <div class="navigation__part">
    <a href="/" class="navigation__item link">Главная</a>

    @auth
      <a href="/ipchecks" class="navigation__item link">Список запросов</a>
    @endauth
  </div>

  <div class="navigation__part">
    @auth
      <span class="navigation__item">{{ auth()->user()->name }}</span>

      <form method="POST" action="/logout">
        @csrf

        <button class="navigation__item link"
          type="submit"
        >Выход</button>
      </form>
    @else
      <a href="/login" class="navigation__item">Логин</a>
      <a href="/register" class="navigation__item">Регистрация</a>
    @endauth
  </div>
</nav>
