<?php

namespace App\Http\Controllers;

use App\Models\IpCheck;
use Illuminate\Http\Request;

class IpChecksController extends Controller
{
  public function index()
  {
    return view('ipchecks.index', [
      'ipChecks' => IpCheck::all()
    ]);
  }

  public function store()
  {
    $attributes = request()->validate([
      'client_ip' => 'required|ip',
      'target_ip' => 'required|ip'
    ]);

    $targetIpData = \SypexGeo::get($attributes['target_ip']);

    IpCheck::create([
      'client_ip' => $attributes['client_ip'],
      'target_ip' => $attributes['target_ip'],
      'city'      => $targetIpData['city']['name_ru'],
      'country'   => $targetIpData['country']['name_ru']
    ]);

    return [
      'city'      => $targetIpData['city']['name_ru'],
      'country'   => $targetIpData['country']['name_ru']
    ];
  }
}
